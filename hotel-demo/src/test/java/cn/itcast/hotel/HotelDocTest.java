package cn.itcast.hotel;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class HotelDocTest {

    @Autowired
    private IHotelService hotelService;

    private RestHighLevelClient client;

    @Before
    public void init(){
        client = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://127.0.0.1:9201")
        ));
    }

    @After
    public void close() throws IOException {
        client.close();
    }

    @Test
    public void createDoc() throws IOException {
//        查询数据库的值
        Hotel hotel = hotelService.getById(36934);

        HotelDoc hotelDoc = new HotelDoc(hotel);
//        把对象转json
        String jsonStr = JSON.toJSONString(hotelDoc);
        System.out.println(jsonStr);

        IndexRequest indexRequest = new IndexRequest("hotel").id(hotel.getId().toString());
        indexRequest.source(jsonStr, XContentType.JSON);
        IndexResponse response = client.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(response.status());

    }


    /**
     * 查询文档
     * @throws IOException
     */
    @Test
    public void getDoc() throws IOException {

        GetRequest getRequest = new GetRequest("hotel").id("36934");
//        发送查询请求
        GetResponse response = client.get(getRequest, RequestOptions.DEFAULT);

//        获取json字符串结果
        String jsonStr = response.getSourceAsString();
        System.out.println(jsonStr);
//        转对象
        HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        System.out.println(hotelDoc);

    }


    /**
     * 修改文档
     * @throws IOException
     */
    @Test
    public void updateDoc() throws IOException {

//  发送更新请求
        UpdateRequest updateRequest = new UpdateRequest("hotel","36934");
        updateRequest.doc("price","300","starName","二钻");
        UpdateResponse response = client.update(updateRequest, RequestOptions.DEFAULT);
        System.out.println(response.status().getStatus());

    }

    /**
     * 修改文档
     * @throws IOException
     */
    @Test
    public void delDoc() throws IOException {


        DeleteRequest delRequest = new DeleteRequest("hotel","36934");
        client.delete(delRequest,RequestOptions.DEFAULT);

    }

    /**
     * 批量导入数据
     * @throws IOException
     */
    @Test
    public void bulkDoc() throws IOException {

//        查询数据库的内容
        List<Hotel> hotelList = hotelService.list();
//        封装批量请求
        BulkRequest bulkRequest = new BulkRequest();
        for (Hotel hotel : hotelList) {
//            把hotel 转换为es的 hotelDoc
            HotelDoc hotelDoc = new HotelDoc(hotel);
//            把对象转json
            String jsonStr = JSON.toJSONString(hotelDoc);
            IndexRequest indexRequest = new IndexRequest("hotel").id(hotelDoc.getId().toString());
//            放入酒店数据
            indexRequest.source(jsonStr,XContentType.JSON);
            bulkRequest.add(indexRequest);
        }

        client.bulk(bulkRequest,RequestOptions.DEFAULT);
    }

    @Test
    public void matchAll() throws IOException {


        SearchRequest searchRequest = new SearchRequest("hotel");


        searchRequest.source().query(QueryBuilders.matchAllQuery());

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }

    }


    @Test
    public void match() throws IOException {


        SearchRequest searchRequest =new SearchRequest("hotel");

        searchRequest.source().query(QueryBuilders.matchQuery("all","外滩").operator(Operator.AND));


        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }
    }


    @Test
    public void multiMatch() throws IOException {


        SearchRequest searchRequest =new SearchRequest("hotel");

        searchRequest.source().query(QueryBuilders.
                multiMatchQuery("外滩","name","city").
                operator(Operator.AND));


        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }
    }

    @Test
    public void testTerm() throws IOException {


        SearchRequest searchRequest =new SearchRequest("hotel");

//        searchRequest.source().query(QueryBuilders.termQuery("name","如家"));
        searchRequest.source().query(QueryBuilders.termQuery("price",336));


        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }
    }


    @Test
    public void testRange() throws IOException {


        SearchRequest searchRequest =new SearchRequest("hotel");

        searchRequest.source().query(QueryBuilders.rangeQuery("price").lte(500).gte(300));


        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }
    }


    @Test
    public void testBool() throws IOException {


        SearchRequest searchRequest =new SearchRequest("hotel");

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        boolQuery.must(QueryBuilders.matchQuery("name","如家"));
        boolQuery.mustNot(QueryBuilders.rangeQuery("price").gte(400));
        boolQuery.filter(QueryBuilders.geoDistanceQuery("location").
                distance("10km").point(new GeoPoint("31.21,121.5")));


        searchRequest.source().query(boolQuery);


        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }
    }


    @Test
    public void sort() throws IOException {


        SearchRequest searchRequest = new SearchRequest("hotel");


        searchRequest.source().query(QueryBuilders.matchAllQuery());

        searchRequest.source().sort("score", SortOrder.DESC);
        searchRequest.source().sort("price", SortOrder.ASC);

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }

    }


    @Test
    public void page() throws IOException {


        SearchRequest searchRequest = new SearchRequest("hotel");


        searchRequest.source().query(QueryBuilders.matchAllQuery());

        int page = 1;//页码
        int index = (page-1) * 3;
        searchRequest.source().from(index);
        searchRequest.source().size(3);

        searchRequest.source().sort("score", SortOrder.DESC);
        searchRequest.source().sort("price", SortOrder.ASC);

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            System.out.println(jsonStr);
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
        }

    }


    @Test
    public void highlight() throws IOException {


        SearchRequest searchRequest =new SearchRequest("hotel");

        searchRequest.source().query(QueryBuilders.matchQuery("all","外滩").operator(Operator.AND));

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("name");
        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.preTags("<em>");
        highlightBuilder.postTags("</em>");

        searchRequest.source().highlighter(highlightBuilder);

        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        SearchHits hits = response.getHits();

        long value = hits.getTotalHits().value;
//        System.out.println("total="+value);

        SearchHit[] hits1 = hits.getHits();
        for (SearchHit hit : hits1) {
            String jsonStr = hit.getSourceAsString();
            HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            if(!CollectionUtils.isEmpty(highlightFields)){
                HighlightField field = highlightFields.get("name");
                Text[] texts = field.getFragments();
                String str = texts[0].toString();
                hotelDoc.setName(str);
            }
            System.out.println(hotelDoc);
        }
    }
}

package cn.itcast.hotel;

import cn.itcast.hotel.pojo.HotelConstants;
import org.apache.http.HttpHost;


import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import java.io.IOException;


public class HotelDemoApplicationTests {


    private RestHighLevelClient client;

    @Before
    public void setUp() {
        this.client = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://127.0.0.1:9201")
        ));
    }

    /**
     * 创建索引库
     */
    @Test
    public void createIndexs() throws IOException {
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("hotel");
        createIndexRequest.source(HotelConstants.MAPPING_TEMPLATE, XContentType.JSON);
        client.indices().create(createIndexRequest, RequestOptions.DEFAULT);
    }

    /**
     * 删除索引库
     * @throws IOException
     */
    @Test
    public void delIndex() throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest("hotel");
        client.indices().delete(deleteIndexRequest,RequestOptions.DEFAULT);
    }

    /**
     * 获取索引库是否存在
     */
    @Test
    public void existIndex() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest("hotel");
        boolean b = client.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
        System.out.println(b);
    }




    @After
    public void tearDown() throws IOException {
        this.client.close();
    }
}

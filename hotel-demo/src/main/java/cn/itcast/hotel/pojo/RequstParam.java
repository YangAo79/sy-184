package cn.itcast.hotel.pojo;

import lombok.Data;

@Data
public class RequstParam {

    private String key;

    private Integer page;

    private Integer size;

    private String sortBy;
}

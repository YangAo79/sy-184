package cn.itcast.hotel.service.impl;

import cn.itcast.hotel.mapper.HotelMapper;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequstParam;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class HotelService extends ServiceImpl<HotelMapper, Hotel> implements IHotelService {

    @Autowired
    private RestHighLevelClient client;

    /**
     * 从Es 进行关键词查询
     * @param requstParam
     * @return
     */
    @Override
    public PageResult search(RequstParam requstParam) {

        String key = requstParam.getKey();
//      创建搜索条件封装对象
        SearchRequest searchRequest = new SearchRequest("hotel");

//        如果没传关键词，就是用match all
        if(StringUtils.isBlank(key)){
            searchRequest.source().query(QueryBuilders.matchAllQuery());
        }else {
//        如果传了关键词
            searchRequest.source().query(QueryBuilders.matchQuery("all", key).operator(Operator.AND));
        }
        int index = (requstParam.getPage() -1) * requstParam.getSize();

//        分页
        searchRequest.source().from(index);
        searchRequest.source().size(requstParam.getSize());
//        排序
        if(StringUtils.isNotBlank(requstParam.getSortBy()) && !"default".equals(requstParam.getSortBy())){
            searchRequest.source().sort(requstParam.getSortBy(), SortOrder.DESC);
        }
        List<HotelDoc> list = new ArrayList<>();
//        发送请求
        try {
            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                String jsonStr = hit.getSourceAsString();
//                对象转换
                HotelDoc hotelDoc = JSON.parseObject(jsonStr, HotelDoc.class);
                list.add(hotelDoc);
            }
            PageResult pageResult = new PageResult(response.getHits().getTotalHits().value,
                    list);
            return pageResult;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("搜索失败");
        }



    }
}

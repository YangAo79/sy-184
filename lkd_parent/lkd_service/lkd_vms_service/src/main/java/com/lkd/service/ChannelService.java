package com.lkd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkd.entity.ChannelEntity;
import com.lkd.http.controller.vo.ChannelConfigReq;
import com.lkd.vo.Pager;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 售货机货道表 服务类
 * </p>
 *
 * @author LKD
 */
public interface ChannelService extends IService<ChannelEntity> {

 /**
     * 根据设备编号获取货道
     * @param innerCode
     * @return
     */
    List<ChannelEntity> getChannelesByInnerCode(String innerCode);


   Boolean channelConfig(ChannelConfigReq channelConfig);

    ChannelEntity getChannelInfo(String innerCode, String channelCode);

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @param searchMap
     * @return
     */
    Pager<ChannelEntity> findPage(long pageIndex, long pageSize, Map searchMap);
}

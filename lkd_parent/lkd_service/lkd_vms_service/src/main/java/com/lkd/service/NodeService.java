package com.lkd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkd.entity.NodeEntity;

/**
 * <p>
 * 点位表 服务类
 * </p>
 *
 * @author LKD
 */
public interface NodeService extends IService<NodeEntity> {

}

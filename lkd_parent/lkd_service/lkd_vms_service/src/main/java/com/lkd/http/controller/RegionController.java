package com.lkd.http.controller;

import com.lkd.entity.RegionEntity;
import com.lkd.http.controller.vo.RegionReq;
import com.lkd.service.RegionService;
import com.lkd.vo.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegionController {

    @Autowired
    private RegionService regionService;

    /**
     * 分页搜索
     * @param name
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping("/region/search")
    public Pager<RegionEntity> search(@RequestParam(name = "name",required = false,defaultValue = "")String name,
                                      @RequestParam(name = "pageIndex",required = false,defaultValue = "1")Long pageIndex,
                                      @RequestParam(name = "pageSize",required = false,defaultValue = "10")Long pageSize){
        return regionService.search(name,pageIndex,pageSize);
    }

    /**
     * 添加区域
     * @param regionReq
     * @return
     */
    @PostMapping("/region")
    public boolean createRegion(@RequestBody RegionReq regionReq){

        return  regionService.createRegion(regionReq);
    }

    /**
     * 修改区域
     * @param regionReq
     * @return
     */
    @PutMapping("/region/{id}")
    public boolean updateRegion(@PathVariable(name = "id")Long regionId,
                                @RequestBody RegionReq regionReq){
        return regionService.updateRegion(regionId,regionReq);
    }

    /**
     * 删除区域
     * @param regionId
     * @return
     */
    @DeleteMapping("/region/{id}")
    public boolean delRegion(@PathVariable(name = "id")Long regionId){
        return regionService.delRegion(regionId);
    }
}

package com.lkd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkd.entity.RegionEntity;
import com.lkd.http.controller.vo.RegionReq;
import com.lkd.vo.Pager;

/**
 * <p>
 * 运营区域 服务类
 * </p>
 *
 * @author LKD
 */
public interface RegionService extends IService<RegionEntity> {

    /**
     * 分页搜索
     * @param name
     * @param pageIndex
     * @param pageSize
     * @return
     */
    Pager<RegionEntity> search(String name, Long pageIndex, Long pageSize);

    /**
     * 添加区域
     * @param regionReq
     * @return
     */
    boolean createRegion(RegionReq regionReq);

    /**
     * 修改区域
     * @param regionId
     * @param regionReq
     * @return
     */
    boolean updateRegion(Long regionId,RegionReq regionReq);

    /**
     * 删除区域
     * @param regionId
     * @return
     */
    boolean delRegion(Long regionId);
}

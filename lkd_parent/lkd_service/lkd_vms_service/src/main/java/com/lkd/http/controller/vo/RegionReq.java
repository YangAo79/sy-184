package com.lkd.http.controller.vo;

import lombok.Data;

@Data
public class RegionReq {

    private String regionName;
    private String remark;
}

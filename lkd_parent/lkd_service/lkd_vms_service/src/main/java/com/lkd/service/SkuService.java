package com.lkd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkd.entity.SkuEntity;
import com.lkd.exception.LogicException;
import com.lkd.vo.Pager;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author LKD
 */
public interface SkuService extends IService<SkuEntity> {

    /**
     * 修改
     * @param skuEntity
     * @return
     */
    boolean updateSku(SkuEntity skuEntity);

    /**
     * 分页查询
     * @param pageIndex 页码
     * @param pageSize 页大小
     * @param classId 商品类别Id
     * @param skuName 商品名称
     * @return 分页结果
     */
    Pager<SkuEntity> search(Long pageIndex, Long pageSize, Long classId, String skuName);
}

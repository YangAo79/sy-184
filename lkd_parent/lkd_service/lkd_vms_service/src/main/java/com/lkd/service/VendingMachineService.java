package com.lkd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lkd.entity.VendingMachineEntity;
import com.lkd.vo.Pager;
import com.lkd.vo.VmVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LKD
 */
public interface VendingMachineService extends IService<VendingMachineEntity> {
 /**
     * 根据状态获取售货机列表
     * @param status
     * @return
     */
    Pager<VendingMachineEntity> search(Long pageIndex, Long pageSize, Integer status, String innerCode);

/**
     * 根据编号查询售货机
     * @param innerCode
     * @return
     */
    VmVO findByInnerCode(String innerCode);

    /**
     * 根据clientId 查询售货机
     * @param clientId
     * @return
     */
    VendingMachineEntity findByClientId(String clientId);

    /**
     * 根据机器状态获取机器编号列表
     * @param isRunning
     * @param pageIndex
     * @param pageSize
     * @return
     */
    Pager<String> getAllInnerCodes(boolean isRunning, long pageIndex, long pageSize);

    /**
     * 更新设备状态(在线离线)
     * @param innerCode 售货机编号
     * @param status 状态
     */
    void updateOnLineStatus(String innerCode, Boolean status);
}

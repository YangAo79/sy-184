package com.lkd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lkd.common.VMSystem;
import com.lkd.dao.VendingMachineDao;
import com.lkd.entity.*;
import com.lkd.service.*;
import com.lkd.vo.Pager;
import com.lkd.vo.VmVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class VendingMachineServiceImpl extends ServiceImpl<VendingMachineDao, VendingMachineEntity> implements VendingMachineService {

/**
     * 查询设备
     * @param pageIndex
     * @param pageSize
     * @param status
     * @return
     */
    @Override
    public Pager<VendingMachineEntity> search(Long pageIndex, Long pageSize, Integer status, String innerCode) {
        Page<VendingMachineEntity> page = new Page<>(pageIndex,pageSize);
        LambdaQueryWrapper<VendingMachineEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(status != null,VendingMachineEntity::getVmStatus,status)
            .likeLeft(StringUtils.isNotBlank(innerCode),VendingMachineEntity::getInnerCode,innerCode);
        Page<VendingMachineEntity> machineEntityPage = this.page(page, queryWrapper);
        return Pager.build(machineEntityPage);
    }

/**
     * 根据编号查询售货机
     * @param innerCode
     * @return
     */
    @Override
    public VmVO findByInnerCode(String innerCode) {

        LambdaQueryWrapper<VendingMachineEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(VendingMachineEntity::getInnerCode,innerCode);
        VendingMachineEntity vendingMachineEntity = getOne(queryWrapper);
        VmVO vmVO = new VmVO();
        if(vendingMachineEntity != null) {
            BeanUtils.copyProperties(vendingMachineEntity, vmVO);
            vmVO.setNodeAddr(vendingMachineEntity.getNode().getAddr());
            vmVO.setNodeName(vendingMachineEntity.getNode().getName());
        }
        return vmVO;
    }

    @Override
    public VendingMachineEntity findByClientId(String clientId) {
        QueryWrapper<VendingMachineEntity> qw = new QueryWrapper<>();
        qw.lambda().eq( VendingMachineEntity::getClientId ,clientId );
        return this.getOne(qw);
    }


    @Override
    public Pager<String> getAllInnerCodes(boolean isRunning, long pageIndex, long pageSize) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<VendingMachineEntity> page = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(pageIndex,pageSize);

        QueryWrapper<VendingMachineEntity> qw = new QueryWrapper<>();
        if(isRunning){
            qw.lambda()
                    .select(VendingMachineEntity::getInnerCode)
                    .eq(VendingMachineEntity::getVmStatus,1);
        }else {
            qw.lambda()
                    .select(VendingMachineEntity::getInnerCode)
                    .ne(VendingMachineEntity::getVmStatus,1);
        }
        this.page(page,qw);
        Pager<String> result = new Pager<>();
        result.setCurrentPageRecords(page.getRecords().stream().map(VendingMachineEntity::getInnerCode).collect(Collectors.toList()));
        result.setPageIndex(page.getCurrent());
        result.setPageSize(page.getSize());
        result.setTotalCount(page.getTotal());

        return result;
    }

    @Override
    public void updateOnLineStatus(String innerCode, Boolean status) {
//        Map statusMap = (Map) redisTemplate.boundHashOps(VMSystem.VM_STATUES_KEY).get(innerCode);
//        if(statusMap==null) {
//            statusMap=new HashMap();
//        }
//        statusMap.put("10001",status);//10001 断网断电   10002 货道   10003 传送轴
//        redisTemplate.boundHashOps(VMSystem.VM_STATUES_KEY).put(innerCode,statusMap);
    }
}

package com.lkd.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.lkd.dao.RegionDao;
import com.lkd.entity.NodeEntity;
import com.lkd.entity.RegionEntity;
import com.lkd.exception.LogicException;
import com.lkd.http.controller.vo.RegionReq;
import com.lkd.service.NodeService;
import com.lkd.service.RegionService;
import com.lkd.vo.Pager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Slf4j
@Service
public class RegionServiceImpl extends ServiceImpl<RegionDao, RegionEntity> implements RegionService {


    /**
     * 分页搜索
     * @param name
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public Pager<RegionEntity> search(String name, Long pageIndex, Long pageSize) {

        if(ObjectUtils.isEmpty(pageIndex)|| pageIndex<1){
            pageIndex = Pager.DEFAULT_PAGE_INDEX;
        }

        if(ObjectUtils.isEmpty(pageSize) || pageSize<1){
            pageSize = Pager.DEFAULT_PAGE_SIZE;
        }
//        分页设置
        Page<RegionEntity> page = new Page<>(pageIndex, pageSize);
//        设置查询条件
        LambdaQueryWrapper<RegionEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(name),RegionEntity::getName,name);

//        分页查询
        Page<RegionEntity> regionEntityPage = page(page, queryWrapper);

        return Pager.build(regionEntityPage);

    }

    /**
     * 添加区域
     * @param regionReq
     * @return
     */
    @Override
    public boolean createRegion(RegionReq regionReq) {

        //1.判断关键数据据
        //   区域名称和区域备注
        if (Strings.isNullOrEmpty(regionReq.getRegionName())
                || Strings.isNullOrEmpty(regionReq.getRemark())
        ) {
            throw new LogicException("关键参数与接口不匹配");
        }
        RegionEntity regionEntity = new RegionEntity();
        regionEntity.setName(regionReq.getRegionName());
        regionEntity.setRemark(regionReq.getRemark());
        // 2.判断业务数据
        //   本次操作没有关联后端（数据库）其他数据，无需操作
        // 3.保持区域数据并返回结果
        return  this.save(regionEntity);
    }

    /**
     * 修改区域
     * @param regionId
     * @param regionReq
     * @return
     */
    @Override
    public boolean updateRegion(Long regionId,RegionReq regionReq) {

//        查询name和remark
        LambdaQueryWrapper<RegionEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(RegionEntity::getName,regionReq.getRegionName()).
                or().
                eq(RegionEntity::getRemark,regionReq.getRemark()).
                ne(RegionEntity::getId,regionId);
        int count = count(queryWrapper);

        if(count>0){
//            存在相同的数据
            log.error("存在相同的数据不能修改！name={}",regionReq.getRegionName());
            throw new LogicException("区域信息已经存在！");
        }

        RegionEntity regionEntity = new RegionEntity();
        regionEntity.setId(regionId);
        regionEntity.setName(regionReq.getRegionName());
        regionEntity.setRemark(regionReq.getRemark());

        return updateById(regionEntity);

    }

    /**
     * 删除区域
     * @param regionId
     * @return
     */
    @Override
    public boolean delRegion(Long regionId) {
        // 1.判断关键数据
        if (ObjectUtils.isEmpty(regionId)) {
            throw new LogicException("关键参数与接口不匹配");
        }

        // 2.判断业务数据
        //  区域信息--判断是否存在
        RegionEntity regionEntity = this.getById(regionId);

        if (regionEntity == null) {
            throw new LogicException("区域信息不存在");
        }

        // 3.修改区域数据并返回结果
        boolean result = this.removeById(regionId);
        return result;
    }
}

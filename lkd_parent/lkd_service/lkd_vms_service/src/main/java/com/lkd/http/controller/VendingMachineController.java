package com.lkd.http.controller;

import com.lkd.entity.VendingMachineEntity;
import com.lkd.entity.VmTypeEntity;
import com.lkd.http.controller.vo.CreateVMReq;
import com.lkd.service.VendingMachineService;
import com.lkd.service.VmTypeService;
import com.lkd.vo.Pager;
import com.lkd.vo.SkuVO;
import com.lkd.vo.VmVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RestController
public class VendingMachineController {

    @Autowired
    private VendingMachineService vendingMachineService;
    @Autowired
    private VmTypeService vmTypeService;
    /**
     * 查询设备
     * @param pageIndex
     * @param pageSize
     * @param status
     * @return
     */
    @GetMapping("/vm/search")
    public Pager<VendingMachineEntity> search(
            @RequestParam(value = "pageIndex",required = false,defaultValue = "1") Long pageIndex,
            @RequestParam(value = "pageSize",required = false,defaultValue = "10") long pageSize,
            @RequestParam(value = "status",defaultValue = "",required = false) Integer status,
            @RequestParam(value = "innerCode",defaultValue = "",required = false) String innerCode){
        return vendingMachineService.search(pageIndex,pageSize,status,innerCode);
    }


    /**
     * 根据编号查询售货机
     * @param innerCode
     * @return
     */
    @GetMapping("/vm/info/{innerCode}")
    public VmVO getByInnerCode(@PathVariable(name = "innerCode")String innerCode){
        return vendingMachineService.findByInnerCode(innerCode);
    }
    /**
     * 根据编号查询售货机
     * @param innerCode
     * @return
     */
    @GetMapping("/vm/vmInfo/{innerCode}")
    public VmVO getvmInfoByInnerCode(@PathVariable(name = "innerCode")String innerCode){
        return vendingMachineService.findByInnerCode(innerCode);
    }

    /**
     * 根据id查询
     * @param id
     * @return 实体
     */
    @GetMapping("/vm/{id}")
    public VendingMachineEntity findById(@PathVariable Long id){
        return vendingMachineService.getById(id);
    }


    @GetMapping("/vm/allTypes")
    public List<VmTypeEntity> getAllType(){
        return vmTypeService.list();
    }

    /**
     * 获取在运行的机器列表
     * @param isRunning
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping("/vm/allByStatus/{isRunning}/{pageIndex}/{pageSize}")
    public Pager<String> getAllByStatus(@PathVariable boolean isRunning,
                                        @PathVariable long pageIndex,
                                        @PathVariable long pageSize){
        return vendingMachineService.getAllInnerCodes(isRunning,pageIndex,pageSize);
    }

    /**
     * 接收设备断连信息
     * @param param
     */
    @PostMapping("/vm/clientAction")
    public void clientAction(@RequestBody Map<String,String> param){
        String clientId = param.get("clientid");  //提取设备id
        log.info("==========clientId={}",clientId);
        log.info("======param={}",param);
        VendingMachineEntity vendingMachine = vendingMachineService.findByClientId(clientId);
        if(vendingMachine==null) {
            return;
        }
        if( param.get("action").equals("client_connected") ){ //如果是联网
            vendingMachineService.updateOnLineStatus( vendingMachine.getInnerCode(), true);
        }
        if( param.get("action").equals("client_disconnected") ){ //如果是断网
            vendingMachineService.updateOnLineStatus( vendingMachine.getInnerCode(), false);
        }
    }
}

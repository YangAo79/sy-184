package com.lkd.http.controller.vo;

import lombok.Data;

@Data
public class SetChannelSkuReq {
    private String innerCode;
    private String channelCode;
    private Long skuId;
}

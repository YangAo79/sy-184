package com.lkd.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lkd.entity.BusinessTypeEntity;
import org.apache.ibatis.annotations.Mapper;


public interface BusinessTypeDao  extends BaseMapper<BusinessTypeEntity> {
}

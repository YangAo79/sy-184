package com.lkd.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lkd.entity.RegionEntity;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 运营区域 Mapper 接口
 * </p>
 *
 * @author LKD
 */
public interface RegionDao extends BaseMapper<RegionEntity> {
    @Results(id = "regionMap",value = {
            @Result(property = "id",column = "id"),
            @Result(property = "nodeCount",column = "id",
                    one = @One(select = "com.lkd.dao.NodeDao.selectCountByRegion"))
    })
    @Select("select * from tb_region where id=#{id}")
    RegionEntity getById(Long id);
}

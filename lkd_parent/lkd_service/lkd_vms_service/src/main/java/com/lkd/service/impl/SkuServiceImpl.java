package com.lkd.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.lkd.dao.SkuDao;
import com.lkd.entity.ChannelEntity;
import com.lkd.entity.SkuClassEntity;
import com.lkd.entity.SkuEntity;
import com.lkd.exception.LogicException;
import com.lkd.service.ChannelService;
import com.lkd.service.SkuClassService;
import com.lkd.service.SkuService;
import com.lkd.vo.Pager;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkuServiceImpl extends ServiceImpl<SkuDao, SkuEntity> implements SkuService {


    /**
     * 修改
     * @param skuEntity
     * @return
     */
    @Override
    public boolean updateSku(SkuEntity skuEntity) {
        UpdateWrapper<SkuEntity> uw = new UpdateWrapper<>();
        uw.lambda()
                .set(skuEntity.getClassId()!=null,SkuEntity::getClassId,skuEntity.getClassId())
                .set(StringUtils.isNotBlank(skuEntity.getSkuName()),SkuEntity::getSkuName,skuEntity.getSkuName())
                .set(StringUtils.isNotBlank(skuEntity.getUnit()),SkuEntity::getUnit,skuEntity.getUnit())
                .set(StringUtils.isNotBlank(skuEntity.getSkuImage()),SkuEntity::getSkuImage,skuEntity.getSkuImage())
                .set(skuEntity.getPrice() != null,SkuEntity::getPrice,skuEntity.getPrice())
                .eq(SkuEntity::getSkuId,skuEntity.getSkuId());

        return this.update(uw);
    }

    /**
     * 分页查询
     * @param pageIndex 页码
     * @param pageSize 页大小
     * @param classId 商品类别Id
     * @param skuName 商品名称
     * @return 分页结果
     */
    @Override
    public Pager<SkuEntity> search(Long pageIndex, Long pageSize, Long classId, String skuName) {

        Page<SkuEntity> page = new Page<>(pageIndex, pageSize);

        LambdaQueryWrapper<SkuEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.
                like(StringUtils.isNotBlank(skuName),SkuEntity::getSkuName,skuName).
                eq(classId != null,SkuEntity::getClassId,classId);

        Page<SkuEntity> page1 = this.page(page, queryWrapper);
        return Pager.build(page1);

    }
}

package com.lkd.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lkd.dao.VendoutRunningDao;
import com.lkd.entity.VendoutRunningEntity;
import com.lkd.service.VendoutRunningService;
import com.lkd.vo.Pager;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class VendoutRunningServiceImpl extends ServiceImpl<VendoutRunningDao, VendoutRunningEntity> implements VendoutRunningService {

}

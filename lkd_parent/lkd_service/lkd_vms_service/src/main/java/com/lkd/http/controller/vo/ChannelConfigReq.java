package com.lkd.http.controller.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 售货机货道配置
 */
@Data
public class ChannelConfigReq implements Serializable {
    private String innerCode;
    private List<SetChannelSkuReq> channelList;
}

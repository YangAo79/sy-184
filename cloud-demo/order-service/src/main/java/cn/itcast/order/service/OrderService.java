package cn.itcast.order.service;

import cn.itcast.order.mapper.OrderMapper;
import cn.itcast.order.pojo.Order;
import cn.itcast.user.client.UserClient;
import cn.itcast.user.pojo.User;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
//@DefaultProperties(defaultFallback = "defaultfallback")
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;
//    @Autowired
//    private RestTemplate restTemplate;
    @Autowired
    private UserClient userClient;


    @HystrixCommand(fallbackMethod = "queryOrderByIdFallback")
    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
//        查询订单的用户数据
        User user = userClient.queryById(order.getUserId());
//        把返回对象设置到订单中
        cn.itcast.order.pojo.User user1 = new cn.itcast.order.pojo.User();
        BeanUtils.copyProperties(user,user1);
        order.setUser(user1);
        // 4.返回
        return order;
    }


//    @HystrixCommand(fallbackMethod = "queryOrderByIdFallback")
//    public Order queryOrderById(Long orderId) {
//        // 1.查询订单
//        Order order = orderMapper.findById(orderId);
////        查询订单的用户数据
//        String url = "http://user-service/user/"+order.getUserId();
//
//        User user = restTemplate.getForObject(url, User.class);
////        把返回对象设置到订单中
//        order.setUser(user);
//        // 4.返回
//        return order;
//    }

    public Order queryOrderByIdFallback(Long orderId){
        return new Order();
    }

    public String defaultfallback(){
        return "错误";
    }
}

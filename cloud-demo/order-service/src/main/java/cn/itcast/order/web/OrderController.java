package cn.itcast.order.web;

import cn.itcast.order.pojo.Order;
import cn.itcast.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.*;

@RestController
@RequestMapping("order")
public class OrderController {

   @Autowired
   private OrderService orderService;

    @GetMapping("{orderId}")
    public Order queryOrderByUserId(@PathVariable("orderId") Long orderId) {
        // 根据id查询订单并返回
        return orderService.queryOrderById(orderId);
    }

        @GetMapping("/test")
        public String test() throws InterruptedException, ExecutionException {
            String result = "";
            // 新建一个线程池
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            // 使用线程池创建任务
            Future<String> future = executorService.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    // 模拟子线程执行耗时
                    Thread.sleep(3000);
                    return "Tom准备完成";
                }
            });
            // 主线程中可以设置最长时间等待
            Thread.sleep(1000);
            // 判断子线程是否执行完成
            if(future.isDone()){
                result = future.get();
            }else{
                // 如果没有完成，返回预先设定好的值，PlanB
                result = "等不及了，换Jerry上吧。";
            }
            return result;
        }
}

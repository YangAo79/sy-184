package cn.itcast.mq.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SpringAmqpListener {

    @RabbitListener(queues = "simple.queue")
    public void receiveMsg(String msg) throws InterruptedException {
        log.info("11111-接收到消息={}",msg);
        Thread.sleep(20);
    }

    @RabbitListener(queues = "simple.queue")
    public void receiveMsg2(String msg) throws InterruptedException {
        log.info("22222-接收到消息={}",msg);
        Thread.sleep(200);
    }


    @RabbitListener(queues = "fanout.queue1")
    public void receiveFanout(String msg) throws InterruptedException {
        log.info("11111-接收到消息={}",msg);
    }

    @RabbitListener(queues = "fanout.queue2")
    public void receiveFanout2(String msg) throws InterruptedException {
        log.info("22222-接收到消息={}",msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "direct.queue1",durable = "true",autoDelete = "false"),
            exchange = @Exchange(name = "directexchange",type = ExchangeTypes.DIRECT),
            key={"blue","red"}))
    public void receiveMsgDirect1(String msg){
        log.info("direct.queue1-接收到消息={}",msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "direct.queue2",durable = "true",autoDelete = "false"),
            exchange = @Exchange(name = "directexchange",type = ExchangeTypes.DIRECT),
            key={"yellow","red"}))
    public void receiveMsgDirect2(String msg){
        log.info("direct.queue2-接收到消息={}",msg);
    }



    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "topic.queue1",durable = "true",autoDelete = "false"),
            exchange = @Exchange(name = "topicexchange",type = ExchangeTypes.TOPIC),
            key={"china.#"}))
    public void receiveMsgTopic1(String msg){
        log.info("direct.queue1-接收到消息={}",msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "topic.queue2",durable = "true",autoDelete = "false"),
            exchange = @Exchange(name = "topicexchange",type = ExchangeTypes.TOPIC),
            key={"#.news"}))
    public void receiveMsgTopic2(String msg){
        log.info("direct.queue2-接收到消息={}",msg);
    }
}

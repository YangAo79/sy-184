package cn.itcast.mq.helloworld;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SpringAmqpTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend(){
        String queueName = "simple.queue";

        String msg = "hello,spring amqp";
        rabbitTemplate.convertAndSend(queueName,msg);
    }


    @Test
    public void testWorkQueue() throws InterruptedException {
        String queueName = "simple.queue";

        for(int i=0;i<50;i++) {
            rabbitTemplate.convertAndSend(queueName, i+"");
            Thread.sleep(20);
        }
    }

    @Test
    public void testFanout() throws InterruptedException {
        String exchange = "fanoutExchange";
        rabbitTemplate.convertAndSend(exchange,"","fanout");
    }


    @Test
    public void testDirect() throws InterruptedException {
        String exchange = "directexchange";
        rabbitTemplate.convertAndSend(exchange,"yellow","direct");
    }

    /**
     * topicExchange
     */
    @Test
    public void testSendTopicExchange() {
        // 交换机名称
        String exchangeName = "topicexchange";
        // 消息
        String message = "喜报！孙悟空大战哥斯拉，胜!";
        Map<String,String> map = new HashMap<>();
        map.put("name","jack");
        // 发送消息
        rabbitTemplate.convertAndSend(exchangeName, "china.weather", map);
    }
}
